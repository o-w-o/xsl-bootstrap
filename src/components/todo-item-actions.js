import React, { Component } from "react";
import PropTypes from "prop-types";
import { TodoItemActionsElm } from "./styled";
import { TODO_STATUS, Todo } from "../domains/todo";

class TodoItemActions extends Component {
  handleTodoItemBeRemovedEvent(id) {
    const { onTodoItemBeRemoved } = this.props;
    onTodoItemBeRemoved(new Todo(id));
  }

  handleTodoItemStatusBeChangedEvent(id, status) {
    const { onTodoItemStatusChange } = this.props;
    onTodoItemStatusChange(new Todo(id, "", status));
  }

  render() {
    const { hasCompleted, id } = this.props;
    return (
      <TodoItemActionsElm>
        <button
          onClick={e => {
            e.preventDefault();
            e.stopPropagation();
            this.handleTodoItemBeRemovedEvent(id);
          }}>
          REMOVE
        </button>

        {hasCompleted === TODO_STATUS.UNDO 
          ? (<button
              onClick={e => {
              e.preventDefault();
              e.stopPropagation();
              this.handleTodoItemStatusBeChangedEvent(
                  id,
                  TODO_STATUS.COMPLETED
              );
            }}>
              DONE
            </button>) 
          : (<button
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
              this.handleTodoItemStatusBeChangedEvent(id, TODO_STATUS.UNDO);
            }}>
              TODO
            </button>)
        }
      </TodoItemActionsElm>
    );
  }
}

export default TodoItemActions;
