import styled, { keyframes } from "styled-components";

export const TodoItemElm = styled.li`
  position: relative;
  margin: 0;
  padding: 12px;
  list-style: none;
  text-align: left;
  user-select: none;
  overflow: hidden;

  &.completed {
    color: grey;

    :after {
      content: "";
      position: absolute;
      left: 0;
      top: 50%;
      width: 95%;
      margin: 0 2.5%;
      border-top: 2px solid rgba(0, 0, 255, 0.25);
    }
  }

  /** 伪类 **/
  :not(:first-child) {
    border-top: 1px solid lightgrey;
  }
`;

const slideFromLeft = keyframes`
    from { transform: translate(30px)};
    to { transform: translate(0px)};
`;

export const TodoItemActionsElm = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  padding: 12px;
  background: #eee;
  border-left: 1px solid #ddd;
  animation: ${slideFromLeft} 0.3s ease-in-out;
  z-index: 9;

  button {
    margin-right: 12px;
  }
`;

export const TodoListElm = styled.ul`
  width: 320px;
  margin: auto;
  padding: 0;
  box-shadow: 1px 1px 1px #888;
`;

export const TodoFilterLinkElm = styled.div`
  margin: auto;
  padding: 12px 0;
  width: 320px;
  display: flex;
  align-items: center;
  justify-content: center;

  button {
    width: 33.3333%;
    outline: none;
    &.active {
      border-color: darkgray;
      background-color: #bcbcbc;
    }
  }
`;

export const TodoRegisterWrapElm = styled.div`
  width: 320px;
  margin: auto;
  padding: 0;
  box-shadow: 1px 1px 1px #888;
`;

export const TodoRegisterElm = styled.form`
  margin-top: 24px;
  padding: 12px;
  display: flex;
  justify-content: center;
  box-shadow: 1px 1px 1px #888;

  input {
    width: calc(100% - 84px);
    margin-right: 24px;
    border: none;
    border-bottom: 2px solid #eee;
    transition: border 0.5s;

    :focus {
      outline: none;
      border-bottom-color: #aaa;
    }
  }

  button {
    background: #fff;
    border: none;
    border-radius: 2px;
    transition: background 0.2s;

    :hover {
      background-color: #eee;
    }

    :focus {
      outline: none;
    }
  }
`;
