import React, { Component } from "react";
import PropTypes from "prop-types";

import { TodoFilterLinkElm } from "./styled";
import { VisibilityFilters } from "../domains/actions";

class TodoFilterLink extends Component {
  constructor(props) {
    super(props);
    this.state = {
      acvtive: 2
    };
  }

  render() {
    const { onTodoItemsVisibilityFilterChange } = this.props;
    console.log(this.props);
    return (
      <TodoFilterLinkElm>
        <button
          className={this.state.acvtive === 1 ? "active" : ""}
          onClick={e => {
            e.preventDefault();
            this.setState({
              acvtive: 1
            });
            onTodoItemsVisibilityFilterChange(VisibilityFilters.SHOW_ACTIVE);
          }}>
          未完成
        </button>

        <button
          className={this.state.acvtive === 2 ? "active" : ""}
          onClick={e => {
            e.preventDefault();
            this.setState({
              acvtive: 2
            });
            onTodoItemsVisibilityFilterChange(VisibilityFilters.SHOW_COMPLETED);
          }}>
          已完成
        </button>

        <button
          className={this.state.acvtive === 3 ? "active" : ""}
          onClick={e => {
            e.preventDefault();
            this.setState({
              acvtive: 3
            });
            onTodoItemsVisibilityFilterChange(VisibilityFilters.SHOW_ALL);
          }}>
          全部项
        </button>
      </TodoFilterLinkElm>
    );
  }
}

TodoFilterLink.propTypes = {
  id: PropTypes.number.isRequired,
  content: PropTypes.string,
  status: PropTypes.symbol
};

export default TodoFilterLink;
