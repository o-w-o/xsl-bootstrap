import React, { Component } from "react";
import PropTypes from "prop-types";
import TodoItem from "./todo-item";
import { TodoListElm } from "./styled";

class TodoList extends Component {
  render() {
    const { todos } = this.props;
    console.log("todos -> %o", todos);
    
    const todoItems = todos.map(todo => (
      <TodoItem
        key={todo.id}
        {...this.props}
        id={todo.id}
        content={todo.content}
        status={todo.status}
        actionbarIsDispalying={todo.actionbarIsDispalying}
      />
    ));

    return <TodoListElm>{todoItems}</TodoListElm>;
  }
}

TodoList.propType = {
  todos: PropTypes.array
};

export default TodoList;
