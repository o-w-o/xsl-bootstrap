import React, { Component } from "react";
import { connect } from "react-redux";
import logo from "./logo.svg";
import "./App.css";
import TodoRegister from "./containers/todo-register";
import { TODO_STATUS } from "./domains/todo";
import VisibleTodoList from "./containers/todo-filter";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tmpTodo: {
        id: -1,
        content: "",
        status: TODO_STATUS.UNDO
      }
    };
  }

  render() {
    const { tmpTodo } = this.state;

    console.log("props -> %o", this.props);
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Todo Noob</h1>
        </header>

        <p className="App-intro">
          To get started, <code>step by step</code> .
        </p>

        <VisibleTodoList />

        <TodoRegister
          id={tmpTodo.id}
          content={tmpTodo.content}
          status={tmpTodo.status}
          onChange={todo => this.handleTodoItemBeResisted(todo)}
        />
      </div>
    );
  }
}

export default connect()(App);
