import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Todo, TODO_STATUS } from "../domains/todo.js";
import { TodoRegisterElm, TodoRegisterWrapElm } from "../components/styled";
import { registerTodoItem } from "../domains/actions.js";

class TodoRegister extends Component {
  constructor(props) {
    super(props);
    // this.state=this.props.state; // 通过props初始化编辑便签子组件的state
    this.handleTodoItemBeResisted = this.handleTodoItemBeResisted.bind(this);
  }

  handleTodoItemBeResisted(e, todoContent) {
    e.preventDefault();
    if (!todoContent.trim()) {
      todoContent = "随机数据 " + Math.round(Math.random() * 10);
    }
    const { dispatch } = this.props;
    const action = registerTodoItem(
      new Todo(new Date().getMilliseconds(), todoContent, TODO_STATUS.UNDO)
    );

    dispatch(action);
    console.log("dispatch end!", action);
  }

  render() {
    const { id, content, status } = this.props;
    let input;

    return (
      <TodoRegisterWrapElm>
        <TodoRegisterElm
          onSubmit={e => {
            this.handleTodoItemBeResisted(e, input.value);
            input.value = "";
          }}
        >
          <input ref={node => (input = node)} />
          <button type="submit">新建</button>
        </TodoRegisterElm>
      </TodoRegisterWrapElm>
    );
  }
}

TodoRegister.propTypes = {
  content: PropTypes.string
};

TodoRegister = connect()(TodoRegister);

export default TodoRegister;
