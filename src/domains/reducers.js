import { combineReducers } from "redux";
import { TODO_STATUS, Todo } from "./todo";
import {
  VisibilityFilters,
  REGISTER_TODO_ITEM_ACTION,
  CHANGE_TODO_ITEM_STATUS_ACTION,
  SET_VISIBILITY_FILTER_ACTION,
  REVORK_TODO_ITEM_ACTION
} from "./actions";

const initTodos = [
  {
    id: 0,
    content: "React 使用",
    status: TODO_STATUS.COMPLETED
  },
  {
    id: 1,
    content: "StyledComponent 使用",
    status: TODO_STATUS.COMPLETED
  },
  {
    id: 3,
    content: "Redux,ReduxSagas 使用",
    status: TODO_STATUS.UNDO
  },
  {
    id: 4,
    content: "RxJS 使用",
    status: TODO_STATUS.UNDO
  },
  {
    id: 5,
    content: "ReactRouter 使用",
    status: TODO_STATUS.UNDO
  },
  {
    id: 6,
    content: "AntD 使用",
    status: TODO_STATUS.UNDO
  },
  {
    id: 7,
    content: "AntV 使用",
    status: TODO_STATUS.UNDO
  }
];

const initialState = {
  visibilityFilter: VisibilityFilters.SHOW_ALL,
  todos: initTodos
};

function todos(state = initialState.todos, action) {
  console.log("action -> %o", action);

  const todos = state;
  switch (action.type) {
    case REGISTER_TODO_ITEM_ACTION:
      const todo = action.todo;
      return [...state, new Todo(todo.id, todo.content, todo.status)];

    case CHANGE_TODO_ITEM_STATUS_ACTION:
      const { id, status } = action.todo;
      let beChangedTodoItemIndex = todos.findIndex(todo => todo.id === id);
      let tmpTodos = [...todos];
      if (beChangedTodoItemIndex >= 0) {
        let tmpTodos = [...todos];
        tmpTodos[beChangedTodoItemIndex].status = status;
        console.log(
          id,
          status,
          beChangedTodoItemIndex,
          tmpTodos[beChangedTodoItemIndex]
        );
      }
      return tmpTodos;

    case REVORK_TODO_ITEM_ACTION:
      return todos.filter(e => e.id != action.todo.id);
      
    default:
      return state;
  }
}

function visibilityFilter(state = VisibilityFilters.SHOW_COMPLETED, action) {
  switch (action.type) {
    case SET_VISIBILITY_FILTER_ACTION:
      return action.filter;
    default:
      return state;
  }
}

const todoApp = combineReducers({
  visibilityFilter,
  todos
});

export default todoApp;
